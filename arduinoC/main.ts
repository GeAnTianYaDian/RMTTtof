
//% color="#FF8C00" iconWidth=50 iconHeight=40
namespace tellotof{

	
       //% block="esp_tellotof [PINX]" blockType="reporter"  
       //% PINX.shadow="dropdown" PINX.options="PINX" PINX.defl="PINX.A"
    export function esp_tellotofget(parameter: any, block: any) {
        let button=parameter.PINX.code;


        Generator.addInclude("esp_tellotofinclude", "#include <TelloTof.h>",true);
		
        //Generator.addInclude("esp_DS1307AS", "#include <Wire.h>",true);

        //Generator.addObject(`esp_DS1307A`,"int",`get_3tof(uint8_t _num);`);
		

        Generator.addSetup("esp_DS1307A1","Serial2.begin(115200, 13, 14, SERIAL_8N1);",true);

        Generator.addCode([`get_3tof(${button})`,Generator.ORDER_UNARY_POSTFIX]);
		
	
		

    }
    
    
   
  
    
}
